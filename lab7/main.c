#include "head.h"

int main(int argc, char **argv) {

    int countgold = 100, pipes[2], pid[NUMBERUNITS];
    int lacmus = 0; 
    pipe(pipes);
    for(int i = 0; i < NUMBERUNITS; i++) {

        
        pid[i] = fork();
        srand(getpid());

        if(pid[i] == -1) {
        	perror("fork");
        	exit(1);
        } else if(pid[i] == 0) {
            /* this is unit */
        	units_work(pipes);
        }
    }
    /* this is parent */
    close(pipes[1]);
    while(countgold > 0){
        //if(i == NUMBERUNITS) i = 0;
        read(pipes[0], &lacmus, sizeof(lacmus));
        if(lacmus == 1){
            countgold -= PORTIONGOLD;
            printf("В шахте осталось [ %d ] золота\n", countgold);
            fflush(stdout);
        }   
    }
    for(int i = 0; i < NUMBERUNITS; i++)
        kill(pid[i], SIGKILL);
    return 0;
}