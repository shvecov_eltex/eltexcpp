#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


#define PORTIONGOLD 10
#define NUMBERUNITS 3
#define PIPEGOLD "./gold"

void units_work(int *p, int flag);