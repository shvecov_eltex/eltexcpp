#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>

int main(int argc, char **argv){

	int x;
	extern long int coub(int x);
	extern long int four(int x);
	void *ext_func;
	long int (*coubx)(int x);
	long int (*fourx)(int x);

	ext_func = dlopen("./libmydynlib.so", RTLD_LAZY);

	if(!ext_func){
		fprintf(stderr,"dlopen() error: %s\n", dlerror());
		exit(1);
	}

	coubx = dlsym(ext_func, "coub");
	fourx = dlsym(ext_func, "four");

	printf("Enter number: ");
	scanf("%i", &x);

	printf("x^3 = %li\n", (*coubx)(x));
	printf("x^4 = %li\n", (*fourx)(x));

	return 0;
}