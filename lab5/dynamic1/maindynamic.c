#include <stdio.h>

int main(int argc, char **argv){

	int x;
	extern long int coub(int x);
	extern long int four(int x);

	printf("Enter number: ");
	scanf("%i", &x);

	printf("x^3 = %li\n", coub(x));
	printf("x^4 = %li\n", four(x));

	return 0;
}