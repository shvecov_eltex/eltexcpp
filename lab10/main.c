#include "func.h"



int main(int argc, char **argv) {

    if(argc == 1) {
        printf("Use instruction: ./lab10 [file1] [file2] ... [file N]\n");
        exit(0);
    }

    FILE *files[argc - 1];
    void *result[argc - 1];
    pthread_t pthreads[argc - 1];

    for(int i = 0; i < (argc - 1); i++) {
        openfile(&files[i], argv[i + 1]);
        pthread_create(&pthreads[i], NULL, control_sum, &files[i]);
    }

    for(int i = 0; i < (argc - 1); i++) {
        pthread_join(pthreads[i], &result[i]);
        fclose(files[i]);
    }

    printresult(result, argc - 1);
    freemem(result, argc - 1);

    return 0;
}
