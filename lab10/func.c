#include "func.h"

void *control_sum(void * arg) {

    int ch;
    int *sum;
    sum = (int *)malloc(sizeof(int));
    *sum = 0;
    while((ch = fgetc(*(FILE **)arg)) != EOF)
        *sum += ch;
    pthread_exit((void *)sum);
}


void openfile(FILE **file, char *namefile) {

    if((*file = fopen(namefile, "r")) == NULL) {
        printf("Ошибка при открытии файла\n");
        exit(1);
    }
    printf("Файл %s открыт\n", namefile);
}

void printresult(void **res, int n) {

    for(int i = 0; i < n; i++) {
        printf("File %i = %d control summ\n", i + 1, *(int *)((void*)res[i]));
    }
}

void freemem(void **mass, int n){

	for(int i = 0; i < n; i++)
		free((void *)mass[i]);
}
