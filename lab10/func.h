#ifndef __FUNC__
#define __FUNC__

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void *control_sum(void * arg);
void openfile(FILE **file, char *namefile);
void printresult(void **res, int n);
void freemem(void **mass, int n);

#endif
