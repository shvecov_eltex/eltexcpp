#include <wait.h>
#include <signal.h>
#include "head.h"

void sighbear();
void sighbee();
void sigctrl();


int main(int argc, char **argv){

	for(int i = 0; i < NUMBERBEE + 1; i++){

		pid[i] = fork();
		srand(getpid());
		if(pid[i] == -1){
			printf("error\n");
			exit(-1);
		}else if(pid[i] == 0 && i == 0){
			/*Это медведь*/
			work_bear(); 
		}else if(pid[i]==0){
			/*Это пчела*/
			work_bee();
		}
	}
/* Это родитель */

	signal(SIGUSR1, sighbear);
	signal(SIGUSR2, sighbee);
	signal(SIGINT, sigctrl);
	for(;;);
	return 0;
}


void sighbear(){

	signal(SIGUSR1,sighbear);
	printf("Медведь поел\t\t\t");
	fflush(stdout);
	barrelhoney-=PORTIONBEAR;
	if(barrelhoney < 0){
		printf("\nМедведю нехватило и он умер...\n");
		for(int i = NUMBERBEE; i >= 0; i--)kill(pid[i], SIGKILL);
			exit(0);
	}
	printf("В бочке [ %d ] меда\n", barrelhoney);
	fflush(stdout);
}

void sighbee(){
	signal(SIGUSR2, sighbee);
	printf("Пчела принесла мед\t\t");
	fflush(stdout);
	barrelhoney+=PORTIONBEE;
	printf("В бочке [ %d ] меда\n", barrelhoney);
	fflush(stdout);
}

void sigctrl(){
	printf("\nПришел пасечник, в бочке %d меда. Пчелы постарались, медведь жив...\n", barrelhoney);
	exit(0);
}
/*───────────────────█████
───────────────████░░░░███████████
──────────────██░░░░░░░░██░░░░░░░░██
────────────██░░░░░░░░░█░░░░░░░░░░░░█
────────────█░░░░░░░░░█░░░░░░░░░░░░░░█
───────────█░░░░░░░░░██░░░░░░░░░░░░░░░█
───────────█░░░░░░░░░█░░░░░░░░░░░░░░░░█
───────────██░░░░░░░░█░░░░░░░░░░░░░░░░█
────────────█░░░░░░░██░░░░░░░░░░░░░░░█
─────────────█░░░░░░█░░░░░░░░░░░░░░░░█
─▓█────────█▓██░░░░░█░░░░░░░░░░░░░░░█
─██────────██─██░░░░█░░░░░░░░░░░░░░█
───█───────█────█░░░█░░░░░░░░░░░░██
───██─────█─────█████░░░░░░░░░░███
────█─██─█────██▒█████░░░░░░░████
───███░░███──█▒▒▒█████████████▒▒▒█
──█░░░░░░░░███▒▒▒▒████▒▒▒▒█████▒▒▒█
─█░░▓█░░▓█░░█▒█▒▒▒████▒▒▒▒▒████▒▒▒▒██
─█░▓░░░░░░▓░▒██▒▒▒████▒▒▒▒▒████▒▒▒▒████
─█░▓▓░░░░▓▓░█▒█▒▒▒████▒▒▒▒▒████▒▒▒▒██
─██░▓▓▓▓▓▓░░███▒▒█████▒▒▒▒▒████▒▒▒██
──██░░░░░░████▒▒▒█████▒▒▒▒█████▒▒▒█
────██████──██▒▒█████▒▒▒▒▒████▒▒██
──────────────██████████████████*/