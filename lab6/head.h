#pragma once
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>

#define PORTIONBEAR 10
#define NUMBERBEE 3
#define PORTIONBEE 4

int barrelhoney = 0;
int statusbear = 1;
pid_t pid[NUMBERBEE + 1];

void work_bear() {

     while(1){
        sleep(PORTIONBEAR);
        kill(getppid(), SIGUSR1);
        sleep(PORTIONBEAR);
    	}
}

int work_bee() {

    while(1) {
        int	ping = rand() % 17;
        sleep(ping);
        kill(getppid(), SIGUSR2);
    }
}