#ifndef __SERVERF__
#define __SERVERF__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <signal.h>
#include <netdb.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "tcpmsg.pb-c.h"

#define N 5 // количество сообщений в очереди
#define K 5 // задержка перед оправкой сообщения о свободной очереди
#define L 7 // задержка перед отправкой сообщения о наличии сообщений
#define TRUE 1
#define MSQ_ID 2025 // ключ очереди
#define MAXBUFF 1024
#define MAXCLIENT 5 // максимальное количество клиентов TCP
#define BROADCAST "192.168.1.255" // широковещательный адресс сети
#define SERVERPORT1 32006 // tcp порт для клиентов первого типа
#define SERVERPORT2 32007 // tcp порт для клиентов второго типа
#define PORT11 32000 // диапазон портов udp для клиентов первого типа
#define PORT12 32005
#define PORT21 31000 // диапазон портов udp для клиентов второго типа 
#define PORT22 31005

struct msgbuf {
	long mtype; // тип сообщения 
	uint8_t mdata[MAXBUFF]; // данные
};

void * udpto_oneclient(void * arg);
void * tcpto_oneclient(void * arg);
void * tcpwrite(void * arg);
void * udpto_twoclient(void * arg);
void * tcpto_twoclient(void * arg);
void * tcpsend(void * arg);
void * msg_qnum(void * arg);
void sigexit();

int msqid; // очередь сообщений

#endif