#include "twoclientf.h"

#include <stdio.h>
#include <sys/msg.h>

int main(int argc, char **argv){

	int bytewrite, result;
	int udpsoc, tcpsoc;
	uint8_t buff[MAXBUFF];
	struct sockaddr_in clientaddr, clienttcpaddr, serveraddr, sender;
	socklen_t len;

	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(atoi(argv[1]));
	clientaddr.sin_addr.s_addr = INADDR_ANY;
	clienttcpaddr.sin_port = INADDR_ANY;
	clienttcpaddr.sin_family = AF_INET;
	clienttcpaddr.sin_addr.s_addr = INADDR_ANY;
	serveraddr.sin_port = htons(PORTSERVER);
	serveraddr.sin_family = AF_INET;

	len = sizeof(sender);
	udpsoc = socket(AF_INET, SOCK_DGRAM, 0);

	bind(udpsoc, (struct sockaddr *)&clientaddr, sizeof(clientaddr));
	srand(getpid());

	while(TRUE){

		recvfrom(udpsoc, buff, MAXBUFF, 0, (struct sockaddr *)&sender, &len);
		printf("Got udp message from server\n");
		fflush(stdout);
		/* Принимает TCP сообщение */
		serveraddr.sin_addr.s_addr = sender.sin_addr.s_addr;
		tcpsoc = socket(AF_INET, SOCK_STREAM, 0);
		bind(tcpsoc, (struct sockaddr *)&clienttcpaddr, sizeof(clienttcpaddr));
		result = connect(tcpsoc, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
		if(result == -1){
			perror("connect");
			exit(1);
		}

		printf("\033[32m...\033[0m\n");
		bzero(buff, MAXBUFF);
		bytewrite = recv(tcpsoc, buff, MAXBUFF, 0);
		Tcpmsg *temp;
		temp = tcpmsg__unpack(NULL, bytewrite, buff);
		printf("Got tcp message from server: %d %d ", temp->pause, temp->lenstr);
		fwrite(temp->str, sizeof(char), temp->lenstr, stdout);
		printf("\n");
		fflush(stdout);
		close(tcpsoc);
		int pause = (rand() % T) + 1;
		printf("Sleep %d sec\n", pause);
		fflush(stdout);
		tcpmsg__free_unpacked(temp, NULL);
		sleep(pause);
		/* */
	}

	return 0;
}