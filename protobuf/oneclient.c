#include "oneclientf.h"

int main(int argc, char **argv){

	if(argc != 2){
		printf("Use ./oneclient [port = 32000..32005]");
		exit(1);
	}

	int udpsoc, tcpsoc, byteread;
	uint32_t lenstr, pause;
	void *buf;
	socklen_t len;
	char buff[MAXBUFF];

	struct sockaddr_in clientaddr, sender, servaddr;

	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(atoi(argv[1]));
	clientaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORTSERVER);

	len = sizeof(sender);
	udpsoc = socket(AF_INET, SOCK_DGRAM, 0);

	bind(udpsoc, (struct sockaddr *)&clientaddr, sizeof(clientaddr));
	srand(getpid());

	while(TRUE){

		byteread = recvfrom(udpsoc, buff, MAXBUFF, 0, (struct sockaddr *)&sender, &len);
		if(byteread <= 0){
			printf("\033[31m...\033[0m\n");
			exit(0);
		}
		
		servaddr.sin_addr.s_addr = sender.sin_addr.s_addr;
		tcpsoc = socket(AF_INET, SOCK_STREAM, 0);
		if((connect(tcpsoc, (struct sockaddr *)&servaddr, sizeof(servaddr))) == -1){
			perror("connect");
			exit(1);
		}
		printf("\033[32m...\033[0m\n");
		printf("Got message from server\n");
		fflush(stdout);
		/* посылаем сообщение через TCP */
		Tcpmsg tcpmsg = TCPMSG__INIT;
		lenstr = (rand() % MAXLENSTR) + 1;
		tcpmsg.lenstr = lenstr;
		pause = (rand() % MAXPAUSE) + 1;
		tcpmsg.pause = pause;
		
		for(int i = 0; i < lenstr; i++){
			buff[i] = (rand() % 25) + 97;
		}

		tcpmsg.str = (char *)malloc(sizeof(char) * lenstr);
		bcopy(buff, tcpmsg.str, sizeof(char)*lenstr);
		unsigned len = tcpmsg__get_packed_size(&tcpmsg);
		buf = malloc(len);
		tcpmsg__pack(&tcpmsg, buf);
		int byteread = send(tcpsoc, buf, len, 0);
		if(byteread == -1){
			perror("send");
			exit(1);
		}
		
		close(tcpsoc);
		printf("Send message tcp to server: %d %d ", tcpmsg.pause, tcpmsg.lenstr);
		fwrite(tcpmsg.str, sizeof(char), tcpmsg.lenstr, stdout);
		//fwrite(buf+2,len,1,stdout);
		printf("\n");
		fflush(stdout);
		free(tcpmsg.str);
		free(buf);
		sleep(pause);
		/* */
	}

	return 0;
}