#include "serverf.h"

int main(int argc, char **argv){

	pthread_t udpto_oc, tcp_oc, udpto_tc, tcp_tc, qnum;

	msqid = msgget(MSQ_ID, IPC_CREAT | IPC_EXCL | 0666);
	if(msqid == -1){
		perror("msgget");
		exit(1);
	}

	signal(SIGINT, sigexit);

/* Поток для отправки соощения клиенту первого типа */
	int result = pthread_create(&udpto_oc, NULL, udpto_oneclient, NULL);
	if(result != 0){
		perror("pthread_create");
		exit(1);
	}
	pthread_detach(udpto_oc);
/* */

/* Поток для приема TCP сообщений от клиента 1-ого типа */
	result = pthread_create(&tcp_oc, NULL, tcpto_oneclient, NULL);
	if(result != 0){
		perror("pthread_create");
		exit(1);
	}
	pthread_detach(tcp_oc);
/* */
	
/* Поток для отправки udp сообщения клиенту второго типа*/
	result = pthread_create(&udpto_tc, NULL, udpto_twoclient, NULL);
	if(result != 0){
		perror("pthread_create");
		exit(1);
	}
	pthread_detach(udpto_tc);
/* */

/* Поток для отправки TCP сообщений клиентам второго типа */
	result = pthread_create(&tcp_tc, NULL, tcpto_twoclient, NULL);
	if(result != 0){
		perror("pthread_create");
		exit(1);
	}
	pthread_detach(tcp_tc);
/* */

	pthread_create(&qnum, NULL, msg_qnum, NULL);
	pthread_detach(qnum);
	for(;;);

	return 0;
}