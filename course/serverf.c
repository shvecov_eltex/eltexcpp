#include "serverf.h"

void *udpto_oneclient(void * arg){
	
	int udpsoc, broad = 1;
	char mess[] = "Free";

	struct msqid_ds buff;
	struct sockaddr_in broadcast;
	udpsoc = socket(AF_INET, SOCK_DGRAM, 0);
	if(udpsoc == -1){
		perror("socket");
		exit(1);
	}

	setsockopt(udpsoc, SOL_SOCKET, SO_BROADCAST, (char *)&broad, sizeof broad);
	broadcast.sin_family = AF_INET;
	broadcast.sin_addr.s_addr = inet_addr(BROADCAST);

	while(TRUE){
		msgctl(msqid, IPC_STAT, &buff);
		if(buff.msg_qnum < N){
			for(int i = PORT11; i < PORT12; i++){
				broadcast.sin_port = htons(i);
				int writebyte = sendto(udpsoc, mess, strlen(mess) + 1, 0, (struct sockaddr *)&broadcast, sizeof(broadcast));
				if(writebyte == -1){
					perror("sendto");
					exit(1);
				}
			}
			printf("Send udp message to oneclient\n");
			fflush(stdout);
			sleep(K);
		}
	}
}

void *tcpto_oneclient(void * arg){

	int tcpsoc, *listener;
	struct sockaddr_in servsock;
	pthread_t tcpclient;

	servsock.sin_family = AF_INET;
	servsock.sin_port = htons(SERVERPORT1);
	servsock.sin_addr.s_addr = INADDR_ANY;

	tcpsoc = socket(AF_INET, SOCK_STREAM, 0);
	bind(tcpsoc, (struct sockaddr *)&servsock, sizeof(servsock)); 

	listen(tcpsoc, MAXCLIENT);

	while(TRUE){
		
		listener = (int *)malloc(sizeof(int));
		*listener = accept(tcpsoc, NULL, NULL);
		pthread_create(&tcpclient, NULL, tcpwrite, listener);
		pthread_detach(tcpclient);
		printf("\033[32m...\033[0m\n");
	}
}

void * tcpwrite(void * arg){

	int bytewrite;
	int iter = *(int *)arg; 
	struct msgbuf temp;
	char buff[MAXBUFF];

	while(TRUE){
		
		bytewrite = recv(iter, buff, MAXBUFF, 0);
		if(bytewrite <= 0){
			printf("Client disconnected\n");
			printf("\033[31m...\033[0m\n");
			return 0;
		}

		printf("Got tcp message from oneclient: ");
		fwrite(buff, sizeof(char), bytewrite, stdout);
		/* кладем сообщение в очередь */
		temp.mtype = 1; 
		bcopy(buff, temp.mtext, bytewrite);
		int result = msgsnd(msqid, &temp, bytewrite, 0);
		if(result == -1){
			perror("msgsnd");
			exit(1);
		}
		bzero(temp.mtext, MAXBUFF);
		/* */
		printf("\n");
		fflush(stdout);
		close(iter);
		free((int *)arg);
	}
}

void * udpto_twoclient(void * arg){

	int udpsoc, broad = 1;
	char mess[] = "have";

	struct msqid_ds buff;
	struct sockaddr_in broadcast;
	udpsoc = socket(AF_INET, SOCK_DGRAM, 0);
	if(udpsoc == -1){
		perror("socket");
		exit(1);
	}

	setsockopt(udpsoc, SOL_SOCKET, SO_BROADCAST, (char *)&broad, sizeof broad);
	broadcast.sin_family = AF_INET;
	broadcast.sin_addr.s_addr = inet_addr(BROADCAST);

	while(TRUE){
		msgctl(msqid, IPC_STAT, &buff);
		if(buff.msg_qnum != 0){
			for(int i = PORT21; i < PORT22; i++){
				broadcast.sin_port = htons(i);
				int writebyte = sendto(udpsoc, mess, strlen(mess) + 1, 0, (struct sockaddr *)&broadcast, sizeof(broadcast));
				if(writebyte == -1){
					perror("sendto");
					exit(1);
				}
			}
			printf("Send udp message to twoclient\n");
			fflush(stdout);
			sleep(L);
		}	
	}
}

void *tcpto_twoclient(void * arg){

	int tcpsoc, *listiner;
	struct sockaddr_in serveraddr;
	pthread_t tcpsendp;

	serveraddr.sin_port = htons(SERVERPORT2);
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = INADDR_ANY;

	tcpsoc = socket(AF_INET, SOCK_STREAM, 0);
	bind(tcpsoc, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
	listen(tcpsoc, MAXCLIENT);

	while(TRUE){

		listiner = (int *)malloc(sizeof(int));
		*listiner = accept(tcpsoc, NULL, NULL);
		pthread_create(&tcpsendp, NULL, tcpsend, listiner);
		pthread_detach(tcpsendp);
		printf("\033[32m...\033[0m\n");
	}
}

void * tcpsend(void * arg){

	int bytewrite;
	int iter = *(int *)arg;
	struct msgbuf temp;

	bytewrite = msgrcv(msqid, &temp, sizeof(temp) - sizeof(long), 0, 0);
	if(bytewrite == -1){
		perror("msgrcv");
		exit(1);
	}

	printf("Send tcp message to twoclient\n");
	fflush(stdout);
	int byteread = send(iter, temp.mtext, bytewrite, 0);
	if(byteread == -1){
		perror("send");
		exit(1);
	}

	close(iter);
	free((int *)arg);
	printf("\033[31m...\033[0m\n");
	return 0;
}

void * msg_qnum(void * arg){

	struct msqid_ds buff;

	while(TRUE){
		msgctl(msqid, IPC_STAT, &buff);
		printf("Queue have \033[32m%lu\033[0m msg\n", (ulong)buff.msg_qnum);
		fflush(stdout);
		sleep(1);
	}
}

void sigexit(){
	msgctl(msqid, IPC_RMID, 0);
	exit(0);
}