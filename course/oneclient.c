#include "oneclientf.h"

int main(int argc, char **argv){

	if(argc != 2){
		printf("Use ./oneclient [port = 32002..32007]");
		exit(1);
	}

	int udpsoc, tcpsoc, lenstr, pause, byteread;
	socklen_t len;
	char buff[MAXBUFF];

	struct sockaddr_in clientaddr, sender, servaddr;

	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(atoi(argv[1]));
	clientaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(PORTSERVER);

	len = sizeof(sender);
	udpsoc = socket(AF_INET, SOCK_DGRAM, 0);

	bind(udpsoc, (struct sockaddr *)&clientaddr, sizeof(clientaddr));
	srand(getpid());

	while(TRUE){

		byteread = recvfrom(udpsoc, buff, MAXBUFF, 0, (struct sockaddr *)&sender, &len);
		if(byteread <= 0){
			printf("\033[31m...\033[0m\n");
			exit(0);
		}
		
		servaddr.sin_addr.s_addr = sender.sin_addr.s_addr;
		tcpsoc = socket(AF_INET, SOCK_STREAM, 0);
		if((connect(tcpsoc, (struct sockaddr *)&servaddr, sizeof(servaddr))) == -1){
			perror("connect");
			exit(1);
		}
		printf("\033[32m...\033[0m\n");
		printf("Got message from server\n");
		fflush(stdout);
		/* посылаем сообщение через TCP */
		lenstr = (rand() % MAXLENSTR) + 1;
		pause = (rand() % MAXPAUSE) + 1;
		sprintf(buff, "%d", pause);
		sprintf((buff + 1), "%d", lenstr);
		for(int i = 2; i < (lenstr + 2); i++){
			buff[i] = (rand() % 25) + 97;
		}

		int byteread = send(tcpsoc, buff, sizeof(char) * (lenstr + 2), 0);
		if(byteread == -1){
			perror("send");
			exit(1);
		}
		
		close(tcpsoc);
		printf("Send message tcp to server: ");
		fwrite(buff, sizeof(char), lenstr + 2, stdout);
		printf("\n");
		fflush(stdout);
		sleep(pause);
		/* */
	}

	return 0;
}