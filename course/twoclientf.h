#ifndef __TWOCLIENTF__
#define __TWOCLIENTF__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MSQID 2020 // ключ очереди
#define MAXBUFF 1024
#define TRUE 1
#define T 5 // задержка перед получением нового udp сообщения
#define PORTSERVER 32007

struct msgbuf {
	long mtype; // тип сообщения 
	char mtext[MAXBUFF]; // данные
};

#endif