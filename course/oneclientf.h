#ifndef __ONECLIENTF__
#define __ONECLIENTF__


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define TRUE 1
#define MAXPAUSE 8 // задержка перед получением нового сообщения
#define MAXLENSTR 8 // максимальная длина строки
#define MAXBUFF 1024
#define PORTSERVER 32006 // порт сервера

#endif