/*
---Швецов Дмитрий---
Задание: написать программу сортировки массива строк
Строки вводить с клавиатуры. Сначала пользователь вводит
кол-во строк потом сами строки. Для тестирования использовать
перенаправление ввода-вывода ($./myprog < test.txt).
Память для строк выделять динамически с помощью функций malloc или calloc.
Ввод данных, сортировку и вывод результатов оформить в виде функций.
Сортировку делать с помощью qsort если возможно, если нет то писать
свой вариант сортировки.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024 // длина строки

char** readMas(int numstr);
void freememmas(char** mas, int count);
void printMas(char** mas, int count);
int getWordnum(char* str);
static int pred(const void *p1, const void *p2);
int move = 0;

int main(int argc, char **argv) {
    
    unsigned int numberstr = 0;
    char** masstr = NULL;
    printf("Please enter number string: ");
    scanf("%u", &numberstr);
    if(numberstr <= 0) {
        printf("numberstr < or = 0. Error\n");
        exit(1);
    }
    masstr = readMas(numberstr);
    printf("--------noSort----------\n");
    printMas(masstr, numberstr);
    printf("---------Sort-----------\n");
    qsort(masstr, numberstr, sizeof(char *), pred);
    printMas(masstr, numberstr);
    printf("------------------\n");
    printf("move = %i", move);
    freememmas(masstr, numberstr);
    masstr = NULL;

    return 0;
}

char** readMas(int numstr) {
    char buffer[MAX_LEN];
    char* p;
    char** mas = NULL;

    mas = (char **)malloc(sizeof(char *)*numstr);
    if(!mas) {
        printf("Memory was not allocated");
        exit(1);
    }
    getchar();
    for(int i = 0; i < numstr; i++) {
        printf("%i: ", i+1);
        for(p = buffer; (*p = getchar())!='\n'; p++);
        *p='\0';
        mas[i] = (char *)malloc(sizeof(char)*strlen(buffer));
        if(!mas) {
            printf("Memory was not allocated");
            exit(1);
        }
        strcpy(mas[i], buffer);
    }
    return mas;
}

void freememmas(char** mas, int count) {
    for(int i = 0; i < count; i++) {
        free(mas[i]);
    }
    free(mas);
}

void printMas(char** mas, int count) {
    for(int i = 0; i<count; i++) {
        printf("%s\n", mas[i]);
    }
}

int getWordnum(char* str) {

    int num = 0;
    char* p = NULL;
    for(p = str; *p!='\0'; p++) {
        if(*p == ' ') ++num;
    }
    ++num;
    return num;
}

static int pred(const void *p1, const void *p2) {
    int x1 = getWordnum(*(char**)p1);
    int x2 = getWordnum(*(char**)p2);
    if(x1 != x2) move++;
    return x1 - x2;
}