#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <arpa/inet.h>

#define DEFAULT_IF "wlp2s0"
#define BUFFMAX 1024
#define ETHER_TYPE 0x0800

#define MAC0 0x74
#define MAC1 0xdf
#define MAC2 0xbf
#define MAC3 0x45
#define MAC4 0x84
#define MAC5 0x41

int main(int argc, char **argv){

	int socketraw, tx_len = 0, sockack;
	char ifname[IF_NAMESIZE], sendbuf[BUFFMAX];

	struct ifreq if_idx;
	struct ifreq if_mac;
	struct ether_header *eh = (struct ether_header *)sendbuf;
	struct sockaddr_ll socket_address;

	strcpy(ifname, DEFAULT_IF);

	if((socketraw = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1){

		perror("socket:");
		exit(1);
	}

	if((sockack = socket(AF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1){

		perror("socket:");
		exit(1);
	}

	if(setsockopt(sockack, SOL_SOCKET, SO_BINDTODEVICE, ifname, IF_NAMESIZE - 1) == -1){

		perror("setsockopt");
		close(sockack);
		exit(1);
	}

	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifname, IF_NAMESIZE - 1);
	if(ioctl(socketraw, SIOCGIFINDEX, &if_idx) < 0){
		perror("SIOCGIFINDEX");
		close(socketraw);
		exit(1);
	}

	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifname, IF_NAMESIZE - 1);
	if(ioctl(socketraw, SIOCGIFHWADDR, &if_mac) < 0){
		perror("SIOCGIFHWADDR");
		close(socketraw);
		exit(1);
	}

	memset(sendbuf, 0, BUFFMAX);

	for(int i = 0; i < 6; i++)
	eh->ether_shost[i] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[i];

	eh->ether_dhost[0] = MAC0;
	eh->ether_dhost[1] = MAC1;
	eh->ether_dhost[2] = MAC2;
	eh->ether_dhost[3] = MAC3;
	eh->ether_dhost[4] = MAC4;
	eh->ether_dhost[5] = MAC5;
	eh->ether_type = htons(ETH_P_IP);
	tx_len += sizeof(struct ether_header);

	sendbuf[tx_len++] = 0x01;
	sendbuf[tx_len++] = 0x02;
	sendbuf[tx_len++] = 0x03;
	sendbuf[tx_len++] = 0x04;

	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	socket_address.sll_halen = ETH_ALEN;

	socket_address.sll_addr[0] = MAC0;
	socket_address.sll_addr[1] = MAC1;
	socket_address.sll_addr[2] = MAC2;
	socket_address.sll_addr[3] = MAC3;
	socket_address.sll_addr[4] = MAC4;
	socket_address.sll_addr[5] = MAC5;

	int byteread = sendto(socketraw, sendbuf, tx_len, 0, (struct sockaddr *)&socket_address, sizeof(struct sockaddr_ll));

	if(byteread < 0){

		printf("Send failed\n");
		exit(1);
	}

	printf("Send data: \033[32m");
	for(int i = 14; i < byteread; i++)
		printf("%02x ", sendbuf[i]);
		printf("\033[0m\n");

	memset(sendbuf, 0, BUFFMAX);
	int bywrite = recvfrom(sockack, sendbuf, BUFFMAX, 0 , NULL, NULL);

	if(bywrite < 0){

		perror("recvfrom:");
		exit(1);
	}

	printf("Got data: \033[32m");
	for(int i = 14; i < byteread; i++)
		printf("%02x ", sendbuf[i]);
		printf("\033[0m\n");

	close(socketraw);
	close(sockack);
	
	return 0;
}