#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ether.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <arpa/inet.h>

#define DEFAULT_IF "wlp2s0"
#define BUFFMAX 1024
#define ETHER_TYPE 0x0800

void exitprogram();
int sockraw, sockclient;

int main(int argc, char **argv){

	int sockopt;
	char ifname[IF_NAMESIZE], buf[BUFFMAX];

	struct ifreq ifopts;
	struct sockaddr_ll socket_address;
	struct ifreq if_idx;

	strcpy(ifname, DEFAULT_IF);
	if((sockraw = socket(PF_PACKET, SOCK_RAW, htons(ETHER_TYPE))) == -1){

		perror("socket:");
		exit(1);
	}

	if((sockclient = socket(PF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1){

		perror("socket:");
		exit(1);
	}

	strncpy(ifopts.ifr_name, ifname, IF_NAMESIZE - 1);
	printf("Default interface: %s\n", ifopts.ifr_name);
	ioctl(sockraw, SIOCGIFFLAGS, &ifopts);

	if(setsockopt(sockraw, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) == -1){

		perror("setsockopt");
		close(sockraw);
		exit(1);
	}

	signal(SIGINT, exitprogram);

	if(setsockopt(sockraw, SOL_SOCKET, SO_BINDTODEVICE, ifname, IF_NAMESIZE - 1) == -1){

		perror("setsockopt");
		close(sockraw);
		exit(1);
	}

	printf("Waiting to recvfrom...\n");

	int flag = 0;

	while(!flag){

		int bytewrite = recvfrom(sockraw, buf, BUFFMAX, 0, NULL, NULL);
		
		if(buf[14] == 0x01 && buf[15] == 0x02 && buf[16] == 0x03 && buf[17] == 0x04){

			printf("Got packet %d bytes\n", bytewrite);
			printf("Data: \033[32m");
			for(int i = 14; i < bytewrite;i++)
			printf("%02x ", buf[i]);
			printf("\033[0m\n");

			
			char sendbuf[BUFFMAX];
			for(int i = 0; i < 6; i++){
				sendbuf[i] = buf[i + 6];
				sendbuf[i + 6] = buf[i];
			}

			for(int i = 11; i < bytewrite; i++)
				sendbuf[i] = buf[i];
			
			memset(&if_idx, 0, sizeof(struct ifreq));
			strncpy(if_idx.ifr_name, ifname, IF_NAMESIZE - 1);
			if(ioctl(sockraw, SIOCGIFINDEX, &if_idx) < 0){
				perror("SIOCGIFINDEX");
				close(sockraw);
				exit(1);
			}

			socket_address.sll_ifindex = if_idx.ifr_ifindex;
			socket_address.sll_halen = ETH_ALEN;
			for(int i = 0; i < 6; i++)
			socket_address.sll_addr[i] = buf[i];

			int byread = sendto(sockclient, sendbuf, bytewrite, 0, (struct sockaddr *)&socket_address, sizeof(struct sockaddr_ll));
			if(byread < 0)
			perror("sendto:");

			printf("Send packet %d bytes\n", byread);
			printf("Data: \033[32m");
			for(int i = 14; i < byread;i++)
			printf("%02x ", sendbuf[i]);
			printf("\033[0m\n");

			flag++;
		}
	}

	return 0;
}

void exitprogram(){

	close(sockclient);
	close(sockraw);
	exit(0);
}