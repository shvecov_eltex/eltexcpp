#ifndef __SERVERFUNC__
#define __SERVERFUNC__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEMENSION 10
#define NUMBEROBJECT 6
#define PORT 2018
#define MAX_CLIENT 10

struct DATACLIENT {
    char namepilot[32];
    int x;
    int y;
    int way;
};

struct COORDINATES {
    int x;
    int y;
};

struct DATA {
    char field[DEMENSION][DEMENSION];
    int count;
} game;

struct PTH_ARG {
    int iter;
    int soc;
    int sociter;
    int status;
};

struct RESULTLIST {
    char name[32];
    int x;
    int y;
};

typedef struct DATA Data;
typedef struct COORDINATES Coordinates;
typedef struct DATACLIENT Dataclient;
typedef struct PTH_ARG Pth_arg;
typedef struct RESULTLIST Resultlist;

void *showfield(void * arg);
int get_empty(Coordinates *m, int x, int y);
void * work_air(void * arg);
int get_freepth(pthread_t * arg);
int get_freelist(int * arg);
int get_freearg(Pth_arg * arg);

int g_countclient;
int g_object;
int g_iterresult;
pthread_t pthcl[MAX_CLIENT];
int clientlist[MAX_CLIENT];
Pth_arg pth_arg[MAX_CLIENT];
Resultlist result[MAX_CLIENT];

#endif