#include "serverfunc.h"

void *showfield(void * arg) {

    while(g_object != 0) {
        system("clear");
        for(int i = 0; i < DEMENSION; i++) {
            for(int j = 0; j < DEMENSION; j++)
                switch(game.field[i][j]) {
                case '0':
                    printf("\033[0m%c", game.field[i][j]);
                    break;
                case 'x':
                    printf("\033[32m%c", game.field[i][j]);
                    break;
                case '1':
                    printf("\033[31m%c", game.field[i][j]);
                    break;
                }
            printf("\n");
        }
        printf("\033[0m\n+---------------------------------------+\n");
        printf("Here %d aircraft\n", g_countclient);
        printf("Here %d object\n", g_object);
        sleep(1);
    }
    system("clear");
    for(int i = 0; i < NUMBEROBJECT; i++)
        printf("Пилот %s нашел цель на x = %d y = %d\n", result[i].name, result[i].x, result[i].y);
    return 0;
}

int get_empty(Coordinates *m, int x, int y) {

    for(int i = 0; i < NUMBEROBJECT; i++)
        if(m[i].x == x && m[i].y == y)
            return 0;
    return 1;
}

void * work_air(void * arg) {

    Pth_arg *temp;
    Dataclient dataclient;
    temp = (Pth_arg *)arg;
    recv(temp->soc, &dataclient, sizeof(dataclient), 0);
    if(game.field[dataclient.y][dataclient.x] == '1') {
        strcpy(result[g_iterresult].name, dataclient.namepilot);
        result[g_iterresult].x = dataclient.x;
        result[g_iterresult].y = dataclient.y;
        g_iterresult++;
        g_object--;
    }
    game.field[dataclient.y][dataclient.x] = 'x';
    switch (dataclient.way) {
    case 1:
        while(dataclient.y != -1) {
            if(game.field[dataclient.y - 1][dataclient.x] == '1') {
                strcpy(result[g_iterresult].name, dataclient.namepilot);
                result[g_iterresult].x = dataclient.x;
                result[g_iterresult].y = dataclient.y - 1;
                g_iterresult++;
                g_object--;
            }
            dataclient.y--;
            game.field[dataclient.y][dataclient.x] = 'x';
            game.field[dataclient.y + 1][dataclient.x] = '0';
            sleep(1);
        }
        game.field[dataclient.y][dataclient.x] = '0';
        break;
    case 2:
        while(dataclient.y != DEMENSION - 1) {
            if(game.field[dataclient.y + 1][dataclient.x] == '1') {
                strcpy(result[g_iterresult].name, dataclient.namepilot);
                result[g_iterresult].x = dataclient.x;
                result[g_iterresult].y = dataclient.y + 1;
                g_iterresult++;
                g_object--;
            }
            dataclient.y++;
            game.field[dataclient.y][dataclient.x] = 'x';
            game.field[dataclient.y - 1][dataclient.x] = '0';
            sleep(1);
        }
        game.field[dataclient.y][dataclient.x] = '0';
        break;
    case 3:
        while(dataclient.x != -1) {
            if(game.field[dataclient.y][dataclient.x - 1] == '1') {
                strcpy(result[g_iterresult].name, dataclient.namepilot);
                result[g_iterresult].x = dataclient.x - 1;
                result[g_iterresult].y = dataclient.y;
                g_iterresult++;
                g_object--;
            }
            dataclient.x--;
            game.field[dataclient.y][dataclient.x] = 'x';
            game.field[dataclient.y][dataclient.x + 1] = '0';
            sleep(1);
        }
        game.field[dataclient.y][dataclient.x] = '0';
        break;
    case 4:
        while(dataclient.x != DEMENSION - 1) {
            if(game.field[dataclient.y][dataclient.x + 1] == '1') {
                strcpy(result[g_iterresult].name, dataclient.namepilot);
                result[g_iterresult].x = dataclient.x + 1;
                result[g_iterresult].y = dataclient.y;
                g_iterresult++;
                g_object--;
            }
            dataclient.x++;
            game.field[dataclient.y][dataclient.x] = 'x';
            game.field[dataclient.y][dataclient.x - 1] = '0';
            sleep(1);
        }
        game.field[dataclient.y][dataclient.x] = '0';
        break;
    }
    send(temp->soc, "Done", sizeof(char) * strlen("Done"), 0);
    close(temp->soc);
    clientlist[temp->sociter] = -1;
    pthcl[temp->iter] = -1;
    temp->status = -1;
    g_countclient--;
    return 0;
}

int get_freepth(pthread_t *arg) {

    for(int i = 0; i < MAX_CLIENT; i++)
        if(arg[i] == -1)
            return i;
    return -5;
}

int get_freelist(int * arg) {

    for(int i = 0; i < MAX_CLIENT; i++)
        if(arg[i] == -1)
            return i;
    return -5;
}

int get_freearg(Pth_arg * arg) {

    for(int i = 0; i < MAX_CLIENT; i++)
        if(arg[i].status == -1)
            return i;
    return -5;
}