#include "clientfunc.h"

int main(int argc, char **argv) {

    if( argc != 3) {
        printf("Use ./client [IP host] [port]");
        exit(1);
    }

    int soc_client, way, maxway;
    int start_x, start_y, status;
    char namepilot[32];
    char answer[10];
    struct sockaddr_in addr_server;
    struct hostent *ipaddr;
    Data data;

    addr_server.sin_family = AF_INET;
    addr_server.sin_port = htons(atoi(argv[2]));
    ipaddr = gethostbyname(argv[1]);
    addr_server.sin_addr.s_addr = *((unsigned long *)ipaddr->h_addr);

    soc_client = socket(AF_INET, SOCK_STREAM, 0);
    if(soc_client == -1){
        perror("socket");
        exit(1);
    }

    status = connect(soc_client, (struct sockaddr *)&addr_server, sizeof(addr_server));
    if(status == -1) {
        perror("connect");
        exit(1);
    }

    printf("Connection completed\n");
    printf("Enter name pilot: ");
    scanf("%s", namepilot);
    srand(getpid());

    start_x = rand() % DEMENSION;
    start_y = rand() % DEMENSION;
    way = 1;
    maxway = 0 + start_y;

    if(maxway < (DEMENSION - start_y)) {
        maxway = DEMENSION - start_y;
        way = 2;
    }

    if(maxway < (0 + start_x)) {
        maxway = 0 + start_x;
        way = 3;
    }

    if(maxway < (DEMENSION - start_x)) {
        maxway = DEMENSION - start_x;
        way = 4;
    }

    data.x = start_x;
    data.y = start_y;
    data.way = way;
    strcpy(data.namepilot, namepilot);

    send(soc_client, &data, sizeof(data), 0);
    int b_read = recv(soc_client, answer, sizeof(answer), 0);

    fwrite(answer, sizeof(char), b_read, stdout);
    printf("\n");
    close(soc_client);
    return 0;
}