#include "serverfunc.h"

int main(int argc, char **argv) {

    int soc_server;
    int x_rand, y_rand;


    Coordinates objects[NUMBEROBJECT];
    pthread_t pth_field;
    struct sockaddr_in addr_serv;

    g_countclient = 0;
    g_object = 0;
    g_iterresult = 0;
    addr_serv.sin_family = AF_INET;
    addr_serv.sin_addr.s_addr = INADDR_ANY;
    addr_serv.sin_port = htons(PORT);

    srand(getpid());
    memset(game.field, '0',sizeof(char) * DEMENSION * DEMENSION);
    soc_server = socket(AF_INET, SOCK_STREAM, 0);
    bind(soc_server, (struct sockaddr *)&addr_serv, sizeof(addr_serv));

    // распологаем объекты в рандомных точках
    for(int i = 0; i < NUMBEROBJECT; i++) {
        do {
            x_rand = rand() % DEMENSION;
            y_rand = rand() % DEMENSION;
            objects[i].x = x_rand;
            objects[i].y = y_rand;
        } while(get_empty(objects, x_rand, y_rand));
        game.field[objects[i].x][objects[i].y] = '1';
        g_object++;
    }

    for(int i = 0; i < MAX_CLIENT; i++) {
        pthcl[i] = -1;
        clientlist[i] = -1;
        pth_arg[i].status = -1;
    }


    pthread_create(&pth_field, NULL, showfield, NULL); // отрисовка поля
    pthread_detach(pth_field);

    listen(soc_server, MAX_CLIENT);

    while(1) {

        int temparg = get_freearg(pth_arg);
        int templist = get_freelist(clientlist);
        if(templist == -5) {
            exit(1);
        }
        clientlist[templist] = accept(soc_server, NULL, NULL);
        g_countclient++;
        int temppth = get_freepth(pthcl);
        if(temppth == -5) {
            exit(1);
        }
        pth_arg[temparg].iter = temppth;
        pth_arg[temparg].soc = clientlist[templist];
        pth_arg[temparg].sociter = templist;
        pth_arg[temparg].status = 0;
        pthread_create(&pthcl[temppth], NULL, work_air, &pth_arg[temparg]);
        pthread_detach(pthcl[temppth]);
    }

    return 0;
}