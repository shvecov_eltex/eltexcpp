#ifndef __CLIENTFUNC__
#define __CLIENTFUNC__

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <netdb.h>
#include <string.h>

#define DEMENSION 10

struct DATA {
    char namepilot[32];
    int x;
    int y;
    int way;
};

typedef struct DATA Data;

#endif