#ifndef __HEAD__
#define __HEAD__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/msg.h>
#include <signal.h>

#define UNITS_TEAM 100
#define RANDOM_KILL 30
#define RANDOM_RESP 30

struct Datateam {
    int kills;
    int unitresp;
};

typedef struct Datateam Datateam;

struct msgbuf {
    long mtype;
    Datateam mtext;
};

void work_team(int que);

#endif