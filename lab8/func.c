#include "head.h"

void work_team(int que) {

    srand(getpid());

    while(1) {
        struct msgbuf data;
        struct Datateam datateam;

        int kills = rand() % RANDOM_KILL;
        int unitsresp = rand() % RANDOM_RESP;

        datateam.kills = kills;
        datateam.unitresp = unitsresp;
        data.mtype = getpid();
        data.mtext = datateam;

        msgsnd(que, &data, sizeof(datateam), 0);
        sleep(2);
    }
}