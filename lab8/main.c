/*
Противостояние нескольких команд. Каждая команда увеличивается на случайное количество
бойцов и убивает случайное количество бойцов участника. Борьба каждой команды реализуется
в отдельном процессе.
*/

#include "head.h"

int main(int argc, char **argv) {

    pid_t Team[2];
    int myQueue;

    myQueue = msgget(IPC_PRIVATE, 0600 | IPC_CREAT);

    for(int i = 0; i < 2; i++) {

        Team[i] = fork();

        if(Team[i] == -1) {
            perror("fork");
            exit(1);
        } else if(Team[i] == 0) {
            // this is team
            work_team(myQueue);
        }
    }

// this is parent
    struct msgbuf requestmsg;
    long team_one, team_two;
    team_one = UNITS_TEAM;
    team_two = UNITS_TEAM;

    while(team_one >= 0 && team_two >= 0) {

        msgrcv(myQueue, &requestmsg, sizeof(Datateam), Team[0], 0);
        team_two -= requestmsg.mtext.kills;
        team_one += requestmsg.mtext.unitresp;
        printf("Team #1 [ %li ] kill:%d regen: %d \n", team_one,
               requestmsg.mtext.kills, requestmsg.mtext.unitresp);

        msgrcv(myQueue, &requestmsg, sizeof(Datateam), Team[1], 0);
        team_one -= requestmsg.mtext.kills;
        team_two += requestmsg.mtext.unitresp;
        printf("Team #2 [ %li ] kill:%d regen: %d \n", team_two,
               requestmsg.mtext.kills, requestmsg.mtext.unitresp);

    }
    if(team_one > team_two)
        printf("Team #1 win\n");
    else
        printf("Team #2 win\n");

    kill(Team[0], SIGKILL);
    kill(Team[1], SIGKILL);

    return 0;
}