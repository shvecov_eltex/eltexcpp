#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv){

	if(argc != 3){
		printf("Invalid number of arguments\n");
		printf("Use ./program [ -file ] [ number ]\n");
		exit(1);
	}
	FILE *file;
	FILE *tempfile;
	int count = 0;

	if((file=fopen(argv[1], "r+"))==NULL){
		printf("The file can not be opened for reading");
		exit(1);
	}
	tempfile = fopen("file.temp", "w+");
	count = atoi(argv[2]);
	int p;

	while((p = fgetc(file))!=EOF){
		fputc(p, tempfile);
		if((p == 32) && (count > 0)){
			fputc(' ', tempfile);
			count--;
		}
	}
	rewind(file);
	rewind(tempfile);
	while((p = fgetc(tempfile))!=EOF){
		fputc(p, file);
	}
	fclose(file);
	fclose(tempfile);
	system("rm file.temp");

	return 0;
}