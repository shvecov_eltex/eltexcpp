#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024

int getSpace(char* str);

int main(int argc, char **argv){

	if(argc != 3){
		printf("Invalid number of arguments\n");
		printf("Use ./program [ -file ] [ number ]\n");
		exit(1);
	}
	FILE *file;
	FILE *resultFile;
	char* resnamefile;
	char buffer[MAX_LEN];

	if((file=fopen(argv[1], "r"))==NULL){
		printf("The file can not be opened for reading");
		exit(1);
	}
	int count = atoi(argv[2]);
	int number = strlen(argv[1])+8;
	resnamefile = (char*)malloc(sizeof(char) * number);
	strcpy(resnamefile, argv[1]);
	strcat(resnamefile, ".result");
	resnamefile[number] = '\0';
	resultFile = fopen(resnamefile, "w");

	while(!feof(file)){
		if(fgets(buffer, MAX_LEN, file))
			if(getSpace(buffer) <= count)
				fputs(buffer, resultFile);
	}

	free(resnamefile);
	fclose(file);
	fclose(resultFile);

	return 0;
}

int getSpace(char* str){

	int space = 0;

	while(*str != '\n'){
		if(*str == ' ')space++;
		str++;
	}
	return space;	
}