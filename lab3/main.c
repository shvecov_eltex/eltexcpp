#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024

struct Purchase{
	char* name; 	//название покупки
	char datebuy[11];	//дата приобритения
	unsigned int value;	//стоимость
	unsigned int quantity;	//количество
};

typedef struct Purchase Purchases;

void readPurchase(Purchases* pur);
void printPurchase(Purchases** pur, int count);
static int pred(const void *p1, const void *p2);

int main(int argc, char **argv){

	unsigned int count = 0;
	Purchases** myPurchase = NULL;
	printf("Please enter number purchase: ");
	scanf("%u", &count);
	myPurchase = (Purchases**)malloc(sizeof(Purchases*)*count);

	for(int i = 0; i < count; i++){
		myPurchase[i] = (Purchases*)malloc(sizeof(Purchases));
		readPurchase(myPurchase[i]);
	}
	printf("----------------------NOSORT------------------------------\n");
	printPurchase(myPurchase, count);
	printf("----------------------SORT--------------------------------\n");
	qsort(myPurchase, count, sizeof(Purchases *), pred);
	printPurchase(myPurchase, count);

	for(int i = 0; i < count; i++){
		free(myPurchase[i]->name);
		free(myPurchase[i]);
	}
	free(myPurchase);
	
	return 0;
}

void readPurchase(Purchases* pur){

	char buffer[MAX_LEN];

	printf("Enter name purchase: ");
	scanf("%s",buffer);
	pur->name = (char*)malloc(sizeof(char)*strlen(buffer));
	strcpy(pur->name, buffer); // pur -> name = buffer (так делать нельзя!)
	printf("Enter date: ");
	scanf("%s", pur->datebuy);
	printf("Enter value: ");
	scanf("%u", &pur->value);
	printf("Enter quantity: ");
	scanf("%u", &pur->quantity);

}

void printPurchase(Purchases** pur, int count){

	printf("+--------------------------------------------------------+\n");
	printf("|Name:\t\tDate:\t\tValue:\t\tQuantity:|\n");
	printf("+--------------------------------------------------------+\n");

	for(int i = 0; i < count; i++){
		printf("|%s\t\t%s\t%u\t\t%u\n", pur[i]->name, pur[i]->datebuy, pur[i]->value, pur[i]->quantity);
	}
	printf("+--------------------------------------------------------+\n");
}

static int pred(const void *p1, const void *p2){

	Purchases * x = *(Purchases**)p1;
	Purchases * y = *(Purchases**)p2;
	char a[2];
	char b[2];
	for(int i = 0; i < 2; i++){
		a[i] = x->datebuy[i+3];
		b[i] = y->datebuy[i+3];
	}
	return strcmp(a,b);

}