#include "func.h"


int main(int argc, char **argv) {

    pid_t teams[COUNTPLAYER];
    int *pidsteams, shm_id, sem_id, pipes[2];
    union semun arg;

// init area
    sem_lock[0].sem_num = sem_unlock[0].sem_num = 0;
    sem_lock[1].sem_num = sem_unlock[1].sem_num = 1;
    sem_lock[0].sem_op = sem_lock[1].sem_op = -1;
    sem_unlock[0].sem_op = sem_unlock[1].sem_op = 1;
    sem_lock[0].sem_flg = sem_unlock[0].sem_flg = 0;
    sem_lock[1].sem_flg = sem_unlock[1].sem_flg = 0;
    arg.val = 1;
    shm_id = shmget(IPC_PRIVATE, sizeof(int) * COUNTPLAYER + 1, IPC_CREAT | 0666);
    sem_id = semget(IPC_PRIVATE, 2, IPC_CREAT | 0666);

    pidsteams = shmat(shm_id, NULL, 0);
    semctl(sem_id, 0, SETVAL, arg);
    semctl(sem_id, 1, SETVAL, arg);
    pidsteams[COUNTPLAYER] = 0;
    pipe(pipes);
//---------------------------------
    for(int i = 0; i < COUNTPLAYER; i++) {

        teams[i] = fork();

        if(teams[i] == -1) {
            perror("fork");
            exit(1);
        }
        if(teams[i] == 0) {
            // this is team
            work_team(pidsteams, sem_id, pipes);
        }
    }
    // this is parent
    close(pipes[1]);
    int tempteams = COUNTPLAYER;
    struct Datakiller datapipe;
    while(tempteams != 1) {
        read(pipes[0], &datapipe, sizeof(datapipe));
        if(datapipe.killer == pidsteams[datapipe.prey] || pidsteams[datapipe.prey] == 0) {
            printf("Игрок \033[36m%d\033[0m никого не убил\n", datapipe.killer);
            fflush(stdout);
        } else {
            kill(pidsteams[datapipe.prey], SIGKILL);
            printf("Игрок %d убил \033[31m%d\033[0m\n", datapipe.killer, pidsteams[datapipe.prey]);
            fflush(stdout);
            pidsteams[datapipe.prey] = 0;
            tempteams--;
        }
    }

    for(int i = 0; i < COUNTPLAYER; i++) {
        kill(teams[i], SIGKILL);
        if(pidsteams[i] != 0)
            printf("Победил игрок \033[32m%d\033[0m\n", pidsteams[i]);
    }

    shmctl(shm_id, IPC_RMID, 0);
    semctl(sem_id, 0, IPC_RMID);

    return 0;
}