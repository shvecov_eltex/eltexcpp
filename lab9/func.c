#include  "func.h"

void work_team(pid_t *pidsft, int semid, int *ps) {

// запись pid процесса в общую область памяти
    semop(semid, &sem_lock[0], 1);
    pidsft[pidsft[COUNTPLAYER]] = getpid();
    pidsft[COUNTPLAYER] += 1;
    semop(semid, &sem_unlock[0], 1);

// основной алгоритм
    close(ps[0]);
    srand(getpid());
    struct Datakiller data;
    while(1) {

        int temp = rand() % (COUNTPLAYER - 1);
        semop(semid, &sem_lock[1], 1);
        data.prey = temp;
        data.killer = getpid();
        write(ps[1], &data, sizeof(data));
        sleep(2);
        semop(semid, &sem_unlock[1], 1);

    }
}