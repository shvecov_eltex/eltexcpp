#ifndef __FUNC__
#define __FUNC__

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <ctype.h>
#include <signal.h>
#include <string.h>

#define COUNTPLAYER 10
#define MAXPIDBUFF 7

struct Datakiller {
    int killer;
    int prey;
};

union semun {
    int val;
    struct semid_ds *buf;
    unsigned short *array;
    struct seminfo *__buf;
};

struct sembuf sem_lock[2];
struct sembuf sem_unlock[2];

void work_team(pid_t *pidsft, int semid, int *ps);

#endif